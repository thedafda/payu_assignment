module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        src: [
          'public/scripts/helpers.js',
          'public/scripts/ui_components/transaction_row.js',
          'public/scripts/ui_components/transaction_list.js',
          'public/scripts/main.js'
        ],
        dest: 'dist/main.min.js'
      }
    },
    cssmin:{
      build: {
        files: {
          'dist/styles.min.css': 'public/styles/*.css'
        }
      }
    },
    processhtml: {
      build: {
        files: {
          'dist/index.html': ['public/index.html']
        }
      }
    },
    jshint: {
      all: ['public/**/*.js']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-processhtml');

  grunt.registerTask('default', ['uglify']);
  grunt.registerTask('build', ['jshint', 'uglify', 'cssmin', 'processhtml']);

};