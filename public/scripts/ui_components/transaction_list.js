(function (win) {

	/**
	 * Function constructor for Transaction LIst
	 * UI Component
	 */
	function TransactionList (rawArr) {
		this.originalArr = rawArr || [];
		this.dataArr = this.originalArr;
		this.currentPageSize = 5;
		this.currentPageNumber = 1;
		this.render();
		this.bindEvents();
	}

	/**
	 * Base tamplate for the Transaction List
	 */
	TransactionList.prototype.htmlTmplt = [
		'<div class="tlist">',
			'<div class="trow title-bar">',
				'<div class="tcol">Payment ID <span sort-by="paymentId">&#8597;</span></div>',
				'<div class="tcol">Order Date <span sort-by="orderDate">&#8597;</span></div>',
				'<div class="tcol">Merchat ID </div>',
				'<div class="tcol">Customer Email </div>',
				'<div class="tcol">Amount <span sort-by="amount">&#8597;</span></div>',
				'<div class="tcol">Payment Status ',
					'<select filter-by="paymentStatus">',
						'<option value="All">All</option>',
						'<option value="Success">Success</option>',
						'<option value="Failed">Failed</option>',
						'<option value="Dropped">Dropped</option>',
						'<option value="Initiated">Initiated</option>',
						'<option value="Refunded">Refunded</option>',
					'</select>',
				'</div>',
			'</div>',
			'<div class="container" container></div>',
			'<div class="pageFilterBox">',
				'<div class="pages" pages></div>',
				'<div class="tperpage"> Transactions Per Page ',
					'<select class="page-size" page-size>',
						'<option value="5">5</option>',
						'<option value="10">10</option>',
						'<option value="15">15</option>',
						'<option value="20">20</option>',
					'</select>',
				'</div>',	
			'</div>',
		'</div>'
	].join('');


	/**
	 * Render function renders the current view according
	 * to current state of the object.
	 */
	TransactionList.prototype.render = function() {
		this.compiledTmplt = this.htmlTmplt.supplant(this.rawObj);
		
		// Setting base template to DOM
		var div = this.element || document.createElement('div');
		div.innerHTML = this.compiledTmplt;

		// Getting Current filter state.
		var select = div.querySelector('select[filter-by]');
		select.value = this.selctedFilter || 'All';

		// Getting Current page size.
		var selectPageSize = div.querySelector('select[page-size]');
		selectPageSize.value = this.currentPageSize || 5;

		if(this.currentSort) {
			div.querySelector('[sort-by="' + this.currentSort + '"]').classList.add('active');	
		}
		
		// Setting up Transaction Row depending on 
		// page number and page size.
		var container = div.querySelector('[container]');
		var startIndex = (this.currentPageNumber - 1) * this.currentPageSize;
		var endIndex = startIndex + this.currentPageSize - 1;
		var tr;
		for(var p = startIndex; p <= endIndex && this.dataArr[p]; p++){
			tr = new TransactionRow(this.dataArr[p]);
			container.appendChild(tr.getElement());	
		}

		// Setting page elements depeding on 
		// current page number and page size
		var totalPages = Math.ceil(this.dataArr.length / this.currentPageSize);
		var pages = div.querySelector('[pages]');
		var pageSpan;
		for(var i = 1; i <= totalPages; i++){
			pageSpan = document.createElement('span');
			pageSpan.innerHTML = i;
			pageSpan.setAttribute('page-no', i);
			if(i === this.currentPageNumber) {
				pageSpan.classList.add('active');
			}
			pages.appendChild(pageSpan);
		}
		this.element = div;
	};

	/**
	 * Getter function for current element
	 */
	TransactionList.prototype.getElement = function() {
		return this.element;
	};

	/**
	 * Function to bind all events 
	 * using event delegation to handle all the events.
	 */
	TransactionList.prototype.bindEvents = function() {
		var that = this;

		/**
		 * Listening for `click` events on the top element 
		 * to do event delegation
		 */
		this.parentClickHandler =  function(e) {
			var target = e.target;
			if(target.matches('span[sort-by]')){
				var sb = target.getAttribute('sort-by');
				that.currentSortOrder = !that.currentSortOrder;
				if(sb) that.sortBy(sb);
			} else if(target.matches('.pages span')){
				var pageNum = target.getAttribute('page-no');
				if(pageNum) that.changePage(pageNum);
			} 
		};
		that.element.addEventListener('click', this.parentClickHandler);

		/**
		 * Listening for `change` events on the top element 
		 * to do event delegation
		 */
		this.parentChangeHandler = function (e) {
			var target = e.target;
			if(target.matches('[filter-by]')){
				var selectedValue = target.options[target.selectedIndex].value;
				if(selectedValue) that.filterBy(selectedValue);
			} else if(target.matches('[page-size]')){
				var pageSize = target.options[target.selectedIndex].value;
				if(pageSize) that.changePageSize(pageSize);
			}
		};
		that.element.addEventListener('change', this.parentChangeHandler);
	};

	/**
	 * function to sort current state of the ui component.
	 * and, rerender the whole component.
	 */
	TransactionList.prototype.sortBy = function(prop, order) {
		prop = prop || this.currentSort;
		order =  order ||  this.currentSortOrder;
		this.dataArr = this.dataArr.sort(function(a, b){
			var bp = parseFloat(b[prop]),
				ap = parseFloat(a[prop]);
			if(order){
				return ap - bp;	
			}
			return bp - ap;
		});
		this.currentSort = prop;
		this.currentSortOrder = order;
		this.render();
	};

	/**
	 * function to filter current state delending on `paymentStatus`.
	 * and, rerender the whole component.
	 */
	TransactionList.prototype.filterBy = function(val) {
		this.selctedFilter = val;
		this.dataArr = this.originalArr.filter(function(a){
			if(val === 'All') {
				return true;
			}
			return a.paymentStatus === val;
		});
		this.currentPageNumber = 1;
		this.render();
	};

	/**
	 * function to change page number.
	 */
	TransactionList.prototype.changePage = function(pageNumber) {
		pageNumber = Number(pageNumber);
		this.currentPageNumber = pageNumber;
		this.render();
	};

	/**
	 * function to change page size.
	 */
	TransactionList.prototype.changePageSize = function(pageSize) {
		pageSize = Number(pageSize);
		this.currentPageSize = pageSize;
		this.render();
	};

	TransactionList.prototype.removeEvents = function() {
		this.element.removeEventListener('click', this.parentClickHandler);
		this.element.removeEventListener('change', this.parentChangeHandler);
	};

	TransactionList.prototype.destory = function() {
		this.removeEvents();
	};

	win.TransactionList = TransactionList;

}(window));