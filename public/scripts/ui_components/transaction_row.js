(function (win) {

	/**
	 * Function constructor for Transaction Row
	 * UI Component
	 */
	function TransactionRow (rawObj) {
		this.rawObj = rawObj;
		this.render();
		this.bindEvents();
	}

	/**
	 * Base tamplate for the Transaction Row
	 */
	TransactionRow.prototype.htmlTmplt = [
		'<div class="trow">',
			'<div class="tcol paymentId">{paymentId}</div>',
			'<div class="tcol orderDate">{orderDate}</div>',
			'<div class="tcol merchentId">{merchatId}</div>',
			'<div class="tcol customerEmail">',
				'<a href="mailto:{customerEmail}">{customerEmail}</a>',
			'</div>',
			'<div class="tcol amount">&#x20B9; {strAmount}</div>',
			'<div class="tcol paymentStatus {pStatusClass}"><span>{paymentStatus}</span></div>',
		'</div>'
	].join('');

	/**
	 * Render function renders the current view according
	 * to current state of the object.
	 */
	TransactionRow.prototype.render = function() {
		this.rawObj.pStatusClass = this.rawObj.paymentStatus.toLowerCase();
		this.rawObj.strAmount = this.rawObj.amount.toFixed(2);

		this.compiledTmplt = this.htmlTmplt.supplant(this.rawObj);
		var div = document.createElement('div');
		div.innerHTML = this.compiledTmplt;
		this.element = div.children[0];
	};

	/**
	 * Getter function for current element
	 */
	TransactionRow.prototype.getElement = function() {
		return this.element;
	};

	/**
	 * Function binding events
	 */
	TransactionRow.prototype.bindEvents = function() {};

	/**
	 * Function removing events binded.
	 */
	TransactionRow.prototype.removeEvents = function() {};

	/**
	 * Function destory to destory current events.
	 */
	TransactionRow.prototype.destory = function() {
		this.removeEvents();
	};

	win.TransactionRow = TransactionRow;

}(window));