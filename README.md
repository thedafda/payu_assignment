# PayU Assignment #

For Development version.
   Open `public/index.html`

For Production version(js and css files are minified).
   Open `dist/index.html`

Grunt commands to build production version in `dist` folder: `grunt build`  
 
Production Deployment Link-
https://s3-ap-southeast-1.amazonaws.com/payu.assignment/index.html